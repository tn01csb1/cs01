CC            := gcc
CFLAGS        := -w -Wl,-subsystem,windows   
INCLUDE_PATHS := \mingw_dev_lib\include
CFLAGS        += $(addprefix -I,$(INCLUDE_PATHS))
LIBRARY_PATHS := \mingw_dev_lib\lib
LDFLAGS       := $(addprefix -L,$(LIBRARY_PATHS))
LDLIBS        := -lmingw32 -lSDLmain -lSDL -lSDL_image -lSDL_ttf -lSDL_mixer

SRCDIR := src
SRC    := $(wildcard $(SRCDIR)/*.c)
EXEDIR := exe
EXE    := $(EXEDIR)/game

.PHONY: all

all : $(EXE)

$(EXE): $(SRC) | $(EXEDIR)
	$(CC) $(CFLAGS) $(LDFLAGS) $^ -o $@ $(LDLIBS)

$(EXEDIR):
	mkdir -p $@