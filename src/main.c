#include <stdio.h>
#include "obj_oil.h"
#include "obj_car.h"
#include "obj_powerup.h"

// Default values imports
#include "main.h"

// SDL Includes
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <SDL/SDL_mixer.h>
//#include <windows.h>

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
const int SCREEN_BPP = 32;
const int opponent_pos[] = {160, 210, 260, 310, 360, 440};

// scoring system
char  scorebanner[100];
char  levelbanner[100];
char  collisionbanner[] = "Show me your license!";
char  quitbanner[] = "Press ESCAPE to quit !";
unsigned int score = 0;
unsigned int level = 1;
unsigned int bg_level_acceleration = 0;
unsigned int bg_max_gear    = 4;

//Initialisation of the surface to NULL
SDL_Surface * car = NULL;
SDL_Surface * opponent1 = NULL;
SDL_Surface * opponent2 = NULL;
SDL_Surface * opponent3 = NULL;
SDL_Surface * screen = NULL;
SDL_Surface * bgTrack = NULL;
SDL_Surface * message = NULL;
TTF_Font *font = NULL;
TTF_Font *font2 = NULL;
//Setting the text colour to white.
SDL_Color textColor = { 255, 255, 255 };
SDL_Color textColorRed = { 255, 0, 0 };

//The music that will be played
Mix_Music *music = NULL;

//The sound effects that will be used
Mix_Chunk *scratch = NULL;

Mix_Chunk *level_up = NULL;

//The frame rate doesn't work for some reason 
//const int FRAMES_PER_SECOND = 20;

//car objects fron obj_car.h
obj_car myCar, opponentCar1, opponentCar2, opponentCar3;


//Essentials in SDL
SDL_Event event;
Uint32 colorKey;
Uint8 *keystate;


//putting the new surface on the old one.
void applySurface (int x, int y, SDL_Surface* src, SDL_Surface* dest)
{

    SDL_Rect offset;

    offset.x = x;
    offset.y = y;

    SDL_BlitSurface (src, NULL, dest, &offset);
}


//Initialising the environment
int init()
{
    //Init
    if ( SDL_Init ( SDL_INIT_EVERYTHING ) == -1 ) {
        return FALSE;
    }

    //Setup of screen resolution
    screen = SDL_SetVideoMode ( SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, SDL_SWSURFACE );

    //ERRORs
    if ( screen == NULL ) {
        return FALSE;
    }
     if( Mix_OpenAudio( 22050, MIX_DEFAULT_FORMAT, 2, 4096 ) == -1 )
    {
        return FALSE;    
    }
    //Name of the window
    SDL_WM_SetCaption ( "Car Racing Game", NULL );

    
    //NO error
    return TRUE;
}
int load_files()
{
//Loading Fonts

        char buffer[1024];
        char target[2048];

        //get the full path to the current working directory
        getcwd(buffer, 1024);
        printf("cwd\t: %s \n", buffer);
        // This will chop off the directory at the end of the path (doing ../source) effectively)
        dirname(buffer);
        printf("parent\t: %s \n", buffer);

        //Take the path we have ".../SDL2_thing" and build a string appending the rest
        sprintf(target, "%s/assets/fonts/DejeVuSans.ttf", buffer);
        printf("target\t: %s \n", target);






    TTF_Init();
    font = TTF_OpenFont ( "../assets/fonts/DejaVuSans.ttf", 24 );
    font2 = TTF_OpenFont ( "../assets/fonts/DejaVuSans.ttf", 16 );

//ERROR with fonts(removed)
    if ( font == NULL ) {
        return FALSE;
    }

    music = Mix_LoadMUS( "../assets/sound/bg_music.wav" );
    
    //If there was a problem loading the music
    if( music == NULL )
    {
        return FALSE;    
    }

    //Load the sound effects
    scratch = Mix_LoadWAV( "../assets/sound/crashing_car.wav" );

    level_up = Mix_LoadWAV("../assets/sound/level_up.wav");

    //If there was a problem loading the sound effects
    if( scratch == NULL || level_up == NULL )
    {
        return FALSE;    
    }
    //No error!
    return TRUE;
}

int quitEverything(){
    //clean up everythings
    SDL_FreeSurface (bgTrack);
    SDL_FreeSurface (car);
    SDL_FreeSurface(opponent1);
    SDL_FreeSurface(opponent2);
    SDL_FreeSurface(opponent3);
    //Free the sound effects
    Mix_FreeChunk( scratch );
    //Mix_FreeChunk( high );
    Mix_FreeChunk( level_up );
    
    //Free the music
    Mix_FreeMusic( music );
    
    //Close the font
    TTF_CloseFont( font );
    
    //Quit SDL_mixer
    Mix_CloseAudio();
    
    //Quit SDL_ttf
    TTF_Quit();
    // score=0;
    // level=0;
    // bg_max_gear=0;
    // bg_level_acceleration=0;
    //Quit SDL
    SDL_Quit();
    return TRUE;
}

int main (int argc, char * argv[])
{
    int i, j = 0;
    int quit = FALSE;
    int stop_blit = FALSE;
    //int restart = FALSE;
    SDL_Rect carOffset, opponentOffset1, opponentOffset2, opponentOffset3;

    //stuff in the background
    int bgX = 0, bgY = 0;
    int bg_acceleration = 0;
    keystate = SDL_GetKeyState (NULL);
    //start:
    carOffset.x = myCar.x = 307;
    carOffset.y = myCar.y = 390;
    carOffset.h =  80;
    carOffset.w = 40;
    myCar.speedY = 30;

    opponentOffset1.x = opponentCar1.x = 160 ;
    opponentOffset1.y = opponentCar1.y = -100;
    opponentOffset1.w = 40;
    opponentOffset1.h = 80;
    opponentOffset2.x = opponentCar2.x = 210;
    opponentOffset2.y = opponentCar2.y = -200;
    opponentOffset2.w = 40;
    opponentOffset2.h = 80;
    opponentOffset3.x = opponentCar3.x = 260;
    opponentOffset3.y = opponentCar3.y = -300;
    opponentOffset3.w = 40;
    opponentOffset3.h = 80;

    //ERROR in init of screen
    if (init() == FALSE ) {
        printf ("Initialisation of screen failed...!! check init() function... ");
        return 1;
    }//Same but for files
    if (load_files() == FALSE) {
        printf ("Load failed.");
        return 1;
    }

    //loading stuff from ../assets folder
    bgTrack = IMG_Load ("../assets/images/trac.png");
    car   = SDL_DisplayFormat (IMG_Load ("../assets/images/car.bmp") );
    opponent1 = SDL_DisplayFormat (IMG_Load ("../assets/images/traffic0.bmp") );
    opponent2 = SDL_DisplayFormat (IMG_Load ("../assets/images/traffic1.bmp") );
    opponent3 = SDL_DisplayFormat (IMG_Load ("../assets/images/traffic2.bmp") );

    if( Mix_PlayMusic( music, -1 ) == -1 )
            {
                return 1;
            }    

    //If the image was optimized just fine
    if ( car != NULL ) {
        //Map the color key
        colorKey = SDL_MapRGB ( car->format, 0XFF, 0xFF, 0xFF );
        //Set all pixels of color R 0xFF, G 0xFF, B 0xFF to be transparent
        SDL_SetColorKey ( car, SDL_SRCCOLORKEY, colorKey );
    }
    if ( opponent1 != NULL ) {
        //Map the color key
        colorKey = SDL_MapRGB ( opponent1->format, 0XFF, 0xFF, 0xFF );
        //Set all pixels of color R 0xFF, G 0xFF, B 0xFF to be transparent
        SDL_SetColorKey ( opponent1, SDL_SRCCOLORKEY, colorKey );
    }
    if ( opponent2 != NULL ) {
        //Map the color key
        colorKey = SDL_MapRGB ( opponent2->format, 0XFF, 0xFF, 0xFF );
        //Set all pixels of color R 0xFF, G 0xFF, B 0xFF to be transparent
        SDL_SetColorKey ( opponent2, SDL_SRCCOLORKEY, colorKey );
    }
    if ( opponent3 != NULL ) {
        //Map the color key
        colorKey = SDL_MapRGB ( opponent3->format, 0XFF, 0xFF, 0xFF );
        //Set all pixels of color R 0xFF, G 0xFF, B 0xFF to be transparent
        SDL_SetColorKey ( opponent3, SDL_SRCCOLORKEY, colorKey );
    }



    while (!quit) {
        keystate = SDL_GetKeyState (NULL);
        if(keystate[SDLK_SPACE]){
            
        }

        //polling an event to happen
        while (SDL_PollEvent (&event) ) {
            if (event.type == SDL_QUIT) {
                quitEverything();
            }
        }
        //}

        if(keystate[SDLK_ESCAPE]){
            quitEverything();
        }/*
        if (keystate[SDLK_SPACE]) {

                //printf("SPACE WAS PRESSED outside the loop\n");
                score=0;
                level=0;
                bg_acceleration=0;
                bg_max_gear=4;
                bg_level_acceleration=0;
               goto start;

        }*/
        if (keystate[SDLK_UP]) {

            //++i;
            //if (i == 20) {
                //bg_acceleration += bg_acceleration < bg_max_gear ? 1 : 0;
                if(bg_acceleration<bg_max_gear)
                    bg_acceleration=bg_acceleration+1;
                //i = 0;
            //}
            //printf ("BOOO....up key pressed ");

        }
        if (keystate[SDLK_DOWN]) {
            //++j;
            //if (j == 12) {

                bg_acceleration -= bg_acceleration > 0 && bg_acceleration > bg_level_acceleration ? 1 : 0;
                //printf ("BOOO....down key pressed ");
                //j = 0;
            //}

        }
        if (keystate[SDLK_LEFT]) {

            myCar.x -=  myCar.x > 160 ? 2 : 0;
            //printf ("BOOO....left key pressed ");

        }
        if (keystate[SDLK_RIGHT]) {

            myCar.x += myCar.x < 455 ? 2 : 0;
            //printf ("BOOO....right key pressed ");

        }

        //}

        bgY += bg_acceleration + 2;

        //If the background has gone too far
        if ( bgY >= bgTrack->h ) {
            //Reset the offset
            bgY = 0 ;
        }

        //Show the background
        if (!stop_blit) {
            applySurface ( bgX, bgY, bgTrack, screen );
            //        apply_surface( bgX + background->w, bgY, background, screen );
            applySurface ( bgX, bgY - bgTrack->h, bgTrack, screen );
            //score += bg_acceleration * 10 + 5;
            score=score+bg_acceleration*10+5;
            //if (score % 5000 == 0)
            if(score>10000*level){
                level += 1;
                ++bg_level_acceleration;
                ++bg_max_gear;
                if( Mix_PlayChannel( -1, level_up, 0 ) == -1 )
                    {
                        return 1;    
                    }

            }
            sprintf (scorebanner, "Score:%d", score);
            sprintf (levelbanner, "Level:%d", level);
            message = TTF_RenderText_Solid ( font, scorebanner, textColor );
            applySurface ( 0, 50, message, screen );
            message = TTF_RenderText_Solid ( font, levelbanner, textColor );
            applySurface ( 0, 80, message, screen );
            message = TTF_RenderText_Solid ( font2, quitbanner, textColorRed );
            TTF_SetFontStyle(font2, TTF_STYLE_BOLD);
            applySurface ( 440, 0, message, screen );
        }
        //Show the dot
        if (opponentCar1.y > 480) {
            opponentCar1.x = opponent_pos[rand() % 6] ;
            opponentCar1.y = -100 + rand()%50;
        }
        if (opponentCar2.y > 480) {
            opponentCar2.x = opponent_pos[rand() % 6] ;
            opponentCar2.y = -100 + rand()%50;
        }
        if (opponentCar3.y > 480) {
            opponentCar3.x = opponent_pos[rand() % 6] ;
            opponentCar3.y = -100 + rand()%50;
        }
        opponentCar1.y += bg_max_gear - 3 + bg_acceleration ;
        opponentCar2.y += bg_max_gear - 3 + bg_acceleration;
        opponentCar3.y += bg_max_gear - 3 + bg_acceleration;
        carOffset.x = myCar.x ;
        carOffset.y = myCar.y ;
        opponentOffset1.x = opponentCar1.x ;
        opponentOffset1.y = opponentCar1.y ;
        opponentOffset2.x = opponentCar2.x ;
        opponentOffset2.y = opponentCar2.y ;
        opponentOffset3.x = opponentCar3.x ;
        opponentOffset3.y = opponentCar3.y ;
        if (check_collision (carOffset, opponentOffset1) && stop_blit != TRUE) {
            stop_blit = TRUE;
            //printf("Collision detected");
            SDL_FreeSurface (car);
            SDL_FreeSurface (opponent1);

            car   = SDL_DisplayFormat (IMG_Load ("../assets/images/car_damaged.bmp") );
            opponent1 = SDL_DisplayFormat (IMG_Load ("../assets/images/traffic0_damaged.bmp") );
            if ( car != NULL ) {
                //Map the color key
                colorKey = SDL_MapRGB ( car->format, 0XFF, 0xFF, 0xFF );
                //Set all pixels of color R 0xFF, G 0xFF, B 0xFF to be transparent
                SDL_SetColorKey ( car, SDL_SRCCOLORKEY, colorKey );
            }
            if ( opponent1 != NULL ) {
                //Map the color key
                colorKey = SDL_MapRGB ( opponent1->format, 0XFF, 0xFF, 0xFF );
                //Set all pixels of color R 0xFF, G 0xFF, B 0xFF to be transparent
                SDL_SetColorKey ( opponent1, SDL_SRCCOLORKEY, colorKey );
            }
            Mix_HaltMusic();
            if( Mix_PlayChannel( -1, scratch, 0 ) == -1 )
                    {
                        return 1;    
                    }
            applySurface ( myCar.x, myCar.y, car, screen );
            applySurface (opponentCar1.x , opponentCar1.y , opponent1, screen );
            message = TTF_RenderText_Solid ( font, collisionbanner, textColor );
            applySurface ( 0, 110, message, screen );

            keystate = SDL_GetKeyState (NULL);
           // PlaySound("../assets/sound/music.wav", NULL, SND_ASYNC);
            /*if (keystate[SDLK_SPACE]) {

                //printf("SPACE WAS PRESSED outside the loop\n");
                score=0;
                level=0;
                bg_acceleration=0;
                bg_max_gear=4;
                bg_level_acceleration=0;
               goto start;

        }*/
            SDL_Flip ( screen );
        }
        if (check_collision (carOffset, opponentOffset2) && stop_blit != TRUE) {
            stop_blit = TRUE;
            //printf("Collision detected");
            SDL_FreeSurface (car);
            SDL_FreeSurface (opponent2);
            car   = SDL_DisplayFormat (IMG_Load ("../assets/images/car_damaged.bmp") );
            opponent2 = SDL_DisplayFormat (IMG_Load ("../assets/images/traffic1_damaged.bmp") );
            if ( car != NULL ) {
                //Map the color key
                colorKey = SDL_MapRGB ( car->format, 0XFF, 0xFF, 0xFF );
                //Set all pixels of color R 0xFF, G 0xFF, B 0xFF to be transparent
                SDL_SetColorKey ( car, SDL_SRCCOLORKEY, colorKey );
            }
            if ( opponent2 != NULL ) {
                //Map the color key
                colorKey = SDL_MapRGB ( opponent2->format, 0XFF, 0xFF, 0xFF );
                //Set all pixels of color R 0xFF, G 0xFF, B 0xFF to be transparent
                SDL_SetColorKey ( opponent2, SDL_SRCCOLORKEY, colorKey );
            }
            applySurface ( myCar.x, myCar.y, car, screen );
            applySurface (opponentCar2.x , opponentCar2.y , opponent1, screen );
            Mix_HaltMusic();
            if( Mix_PlayChannel( -1, scratch, 0 ) == -1 )
                    {
                        return 1;    
                    }
           // PlaySound("../assets/sound/music.wav", NULL, SND_ASYNC);
            message = TTF_RenderText_Solid ( font, collisionbanner, textColor );
            applySurface ( 0, 110, message, screen );
            keystate = SDL_GetKeyState (NULL);
            SDL_Flip ( screen );
        }
        if (check_collision (carOffset, opponentOffset3) && stop_blit != TRUE) {
            stop_blit = TRUE;
            //printf("Collision detected");
            SDL_FreeSurface (car);
            SDL_FreeSurface (opponent3);
            car   = SDL_DisplayFormat (IMG_Load ("../assets/images/car_damaged.bmp") );
            opponent3 = SDL_DisplayFormat (IMG_Load ("../assets/images/traffic2_damaged.bmp") );
            if ( car != NULL ) {
                //Map the color key
                colorKey = SDL_MapRGB ( car->format, 0XFF, 0xFF, 0xFF );
                //Set all pixels of color R 0xFF, G 0xFF, B 0xFF to be transparent
                SDL_SetColorKey ( car, SDL_SRCCOLORKEY, colorKey );
            }
            if ( opponent3 != NULL ) {
                //Map the color key
                colorKey = SDL_MapRGB ( opponent3->format, 0XFF, 0xFF, 0xFF );
                //Set all pixels of color R 0xFF, G 0xFF, B 0xFF to be transparent
                SDL_SetColorKey ( opponent3, SDL_SRCCOLORKEY, colorKey );
            }
            applySurface ( myCar.x, myCar.y, car, screen );
            applySurface (opponentCar3.x , opponentCar3.y , opponent3, screen );
            Mix_HaltMusic();
            if( Mix_PlayChannel( -1, scratch, 0 ) == -1 )
                    {
                        return 1;    
                    }
            message = TTF_RenderText_Solid ( font, collisionbanner, textColor );
            applySurface (0, 110, message, screen );
            keystate = SDL_GetKeyState (NULL);
            SDL_Flip ( screen );
        }
        if (!stop_blit) {
            applySurface ( myCar.x, myCar.y, car, screen );
            applySurface (opponentCar1.x , opponentCar1.y , opponent1, screen );
            applySurface (opponentCar2.x , opponentCar2.y , opponent2, screen );
            applySurface (opponentCar3.x , opponentCar3.y , opponent3, screen );

        }
        //Update the screen
        if ( SDL_Flip ( screen ) == -1 ) {
            return 1;
        }

        //hold the breath for some sec
        SDL_Delay (1);


    }
 return TRUE;   
}